HMIN205 Projet de programmation mobile : RENDU INTERMÉDIAIRE

Sujet choisi : SmartCity
Membres du groupe : Thomas Georges et Amandine Paillard
Lien du git : https://gitlab.com/amapai/android-ville-connectee

=====================================================================

[ AVANCEMENT ]

Front-end
- Profil utilisateur
- Menu des préférences et intérêts de l'utilisateur
- Fil d'actualité regroupant les différentes informations publiées sur les réseaux, par les commerces, ...
- Bons plans où sont visibles les publicités pour les commerces
- Recherche de commerce (Work in progress)
- Consultation des posts des commerces par catégories
- Recherche de réseaux (Work in progess)
- Consultations des posts dans les réseaux sociaux
- Système de navigation
- Version anglaise de l'application

Back-end 
- Mise à jour de la BD
- Récupération des info de la BD
- Interface permettant la MAJ et la récupération : 
	- Des catégories
	- Des commerces
	- Des lieux
	- De la publicité
	- Des réseaux sociaux (Work in progress)
	- Des utilisateurs


=====================================================================

[ RESTE À FAIRE ]

Attention, le front-end et le back-end ne sont pas encore liés car le serveur n'est pas encore en ligne.

Front-end
- Recherche de réseau : pouvoir rejoindre un réseau
- Recherche de commerces : liés les deux fonctions de recherche différentes et permettre l'affichage des posts d'un commerce
- Ajouter des publications liées à la météo et au trafic dans le fil d'actualité
- Permettre la création de réseaux par l'utilisateur

Back-end
- Actualité
- Mise en ligne du serveur
