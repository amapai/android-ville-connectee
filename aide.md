# Machin pour uniformiser et communiquer dans le projet

## Variable : layout

### Menu de navigation

```
nav_profile : bouton navigation pour accéder à l'activité ProfileActivity
nav_interest : bouton navigation pour accéder à l'activité InterestPreferencesActivity
nav_actu : bouton navigation pour accéder à l'activité NewsActivity
nav_shop_search : bouton navigation pour accéder à l'activité ShopSearchActivity
nav_shop_news : bouton navigation pour accéder à l'activité ShopNewsActivity
nav_networks : bouton navigation pour accéder à l'activité
nav_discover_community : bouton navigation pour accéder à l'activité
nav_good_deal : bouton navigation pour accéder à l'activité
```


## Variable : string

### Globales

app_name : nom de l'application

### Menu de navigation

```
nav_profile : bouton navigation pour accéder à l'activité ProfileActivity
nav_interest : bouton navigation pour accéder à l'activité InterestPreferencesActivity
nav_actu : bouton navigation pour accéder à l'activité NewsActivity
nav_shop : simple label
nav_shop_search : bouton navigation pour accéder à l'activité ShopSearchActivity
nav_shop_news : bouton navigation pour accéder à l'activité ShopNewsActivity
nav_community : simple label
nav_networks : bouton navigation pour accéder à l'activité
nav_discover_community : bouton navigation pour accéder à l'activité
nav_good_deal : bouton navigation pour accéder à l'activité
```


## Activités

* ProfileActivity : voir et éditer le profil utilisateur
* InterestPreferencesActivity : voir et éditer les "préférences" de l'utilisateur
* NewsActivity : fil d'actu
* ShopSearchActivity : recherche de commerces environnant
* ShopNewsActivity : nouvelles liées aux commerces
* NetworksActivity : liste des réseaux de l'utilisateur
* NetworkSearchActivity : recherche de réseaux
* DealsActivity : diverses pubs et promos

## BD
```
UTILISATEUR (id, pseudo, dateNaiss, sexe)
LIEU (id, idContenuPar, logo)
COMMERCE (id, idLieu, nom, idCategorie)
COMMERCE_NEWS (id, idCommerce, contenu)
RESEAU_SOCIAL (id, idLieu, nom, idCategorie, estPublique)
RESEAU_SOCIAL_PUBLICATION (id, idRS, contenu)
PUBLICITE(id, idCategorie, estDispo, logo)
CATEGORIE(id, libele)
CATEGORIE_UTILISATEUR(id, idCategorie, idUser)
COMMERCE_UTILISATEUR(id, idCategorie, idUser)
LIEU_UTILISATEUR(id, idCategorie, idUser)
```
## API à utiliser
```
newUser(pseudo, dateNaiss, sexe): idUser ou -1
connexion(pseudo):idUser ou -1
getLieuUser(int idUser)
ajoutLieuUser(int idUser, int idLieu)
inscription(String pseudo, String dateNaiss, String sexe)

getLieu():String[][] avec une ligne par lieu et une colone par attribut
getCategorie():String[][] avec une ligne par ategorie et une colone par attribut

addNewLieuUser(idUser, idLieu)
addNewCategorieUser(idUser, idLieu)
```

## Fonctions de conversion

```
getIdCategorieByName(str name) : renvoie l'id de la catégorie, -1 sinon
getIdLieuByName(str name) : renvoie l'id du lieu associé, -1 sinon
```

## Fonctions dont j'aurai besoin explicitement

Voici une liste de fonction dont l'UI aurait besoin, tu peux déjà en avoir fait certaine, c'est surtout pour qu'on soit exhaustif !

### Fil d'actu

* getNbCategorie() : renvoie le nombre de catégories figurant dans les préférences de l'user (ce qu'il suit)
* getCategories() : renvoie les libelle des catégories suivies par l'user

### Bon plan

### Réseaux sociaux

*
*
*

### Commerces

### Profil et centre d'intérêt

## 666
 besoin de définir les labels pour les fragments "swipe view"
