package com.example.icity.models;

public class Ad {
    String label;
    String content;

    public Ad(String label, String content) {
        this.label = label;
        this.content = content;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Ad{" +
                "label='" + label + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
