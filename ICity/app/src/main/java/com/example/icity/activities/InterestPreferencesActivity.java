package com.example.icity.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.icity.R;
import com.example.icity.models.API_FOR_MOBILE.Categorie;
import com.example.icity.models.API_FOR_MOBILE.Utilisateur;
import com.example.icity.utils.UtilsActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InterestPreferencesActivity extends UtilsActivity {

    // Variables for interests
    private TableLayout tl;
    private ArrayList<String[]> interests = Categorie.getCategories();
    private TableRow tr;
    private TextView tv;
    private Switch s;
    private Button btnSave;

    /**
     * Called at the creation of the activity. Allow the navigation drawer to be displayed and
     * page adapter.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interest_preferences);

        makeInterestsSuggestion();

        makeToolbar();
        makeNavMenu(InterestPreferencesActivity.this, savedInstanceState); // for navigation drawer
    }


    // *********************************************************************************************
    // ********************************* INTERESTS ************************************************
    // *********************************************************************************************

    /**
     * Display a list of categories. User may follow each one of them.
     */
    public void makeInterestsSuggestion(){
        tl = (TableLayout) findViewById(R.id.interest_list);
        ArrayList<Switch> switches = new ArrayList<>();
        ArrayList<String[]> userInterest;
        ArrayList<String> userInterestLabel = new ArrayList<>();
        userInterest = Utilisateur.getCategorieUser(userId);
        if(userInterest!=null){
            for (String[] uI: userInterest) {
                userInterestLabel.add(uI[0]);
            }
        }
        for(String[] i : interests){
            tr = new TableRow(this);

            tv = new TextView(this);
            tv.setText(i[1]);
            tr.addView(tv);

            s = new Switch(this);
            if(userInterestLabel.contains(i[1])){
                s.setChecked(true);
            }
            switches.add(s);
            tr.addView(s);
            tl.addView(tr);
        }
        final ArrayList<Switch>s = switches;
        btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveUserInterest(interests, s);
            }
        });
    }

    /**
     * Called when user click on save button. Check which interest is
     * selected and save it in server.
     */
    public void saveUserInterest(ArrayList<String[]> interests, ArrayList<Switch> switches){
        ArrayList<Integer> categoriesIdTmp = new ArrayList<>();
        int cpt = 0;
        for (int i = 0 ; i < switches.size() ; i ++){
            // if selected, we need to add the interest to user's list
            if(switches.get(i).isChecked()){
                                categoriesIdTmp.add(Integer.parseInt(interests.get(i)[0]));
                cpt++;
            }
        }
        int categoriesId[] = new int[cpt];
        for(int i = 0 ; i < cpt ; i++) {
            categoriesId[i] = categoriesIdTmp.get(i);
        }
        Utilisateur.setCategorieUser(userId, categoriesId);
        Toast.makeText(InterestPreferencesActivity.this, "Enregistré !",Toast.LENGTH_LONG).show();

    }
}
