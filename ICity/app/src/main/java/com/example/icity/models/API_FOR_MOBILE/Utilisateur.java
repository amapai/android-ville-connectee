
package com.example.icity.models.API_FOR_MOBILE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import com.example.icity.models.API_TO_PHP.Select;
import com.example.icity.models.API_TO_PHP.Test;
import com.example.icity.models.API_TO_PHP.Update;

/**
 * The Class Utilisateur.
 */
public class Utilisateur {

	public static void main(String[] arg){
		/*System.out.println(""+connexion("adminF"));
		System.out.println();
		String [] tmp = getInfoUser(1);
		for(int i = 0; i < tmp.length;i++){
			System.out.print(tmp[i]+" ");
		}
		System.out.println();
		System.out.println();
		Test.afficher(getInfoAllUser());
		
		int[]cat = new int[3];
		cat[0] = 1;
		cat[1] = 2;
		cat[2] = 3;
		System.out.println(setCategorieUser(0,cat));
		System.out.println();
		
		
		//tmp = getCategorieUser(0);
		for(int i = 0; i < tmp.length;i++){
			System.out.print(tmp[i]+" ");
		}*/
	}

	/**
	 * Connecte l'utilisateur .
	 *
	 * @param pseudo le pseudo de l'user
	 * @return l'id de l'utilisateur ou -1 si pb
	 */
	public static int connexion(String pseudo){
		try {
			ArrayList<String[]> ret = new Select().execute("SELECT id FROM utilisateur WHERE pseudo='"+pseudo+"'").get();
			return ret != null?Integer.parseInt(ret.get(0)[0]):-1;
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	/**
	 * 
	 * Renvoi les informations d'un utilisateur.
	 *
	 * @param idUser l'id de l'user
	 * @return ses infos
	 */
	public static String[] getInfoUser(int idUser){
		try {
			return new Select().execute("SELECT * FROM utilisateur WHERE id="+idUser).get().get(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 
	 * Renvoi les info de tout les utilisateurs.
	 *
	 * @return the info all user
	 */
	public static ArrayList<String[]> getInfoAllUser(){
		try {
			return new Select().execute("SELECT * FROM utilisateur").get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 *  Renvoi les catégories de l'utilisateur.
	 *
	 * @param idUser the id user
	 * @return les catégories de l'utilisateur
	 */
	public static ArrayList<String[]> getCategorieUser(int idUser){
		try {
			return new Select().execute("SELECT DISTINCT libele FROM utilisateur "
					+ "JOIN categorie_utilisateur ON categorie_utilisateur.idUser=utilisateur.id "
					+ "JOIN categorie ON categorie_utilisateur.idCategorie = categorie.id "
					+ "WHERE utilisateur.id="+idUser).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return null;
	}

	//
	public static int getLastIdCU(){
		try {
			return Integer.parseInt(new Select().execute("SELECT max(id) FROM categorie_utilisateur").get().get(0)[0]);
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return -1;
	}
	//
	public static int getLastIdU(){
		try {
			return Integer.parseInt(new Select().execute("SELECT max(id) FROM utilisateur").get().get(0)[0]);
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return -1;
	}
	public static int getLastIdVU(){
		try {
			return Integer.parseInt(new Select().execute("SELECT max(id) FROM lieu_utilisateur").get().get(0)[0]);
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return -1;
	}
	/**
	 * 
	 * Enregistre n nouvelles catégories pour l'utilisateur.
	 *
	 * @param idUser the id user
	 * @param categoriesId the categorie
	 * @return true, if successful
	 */
	public static boolean setCategorieUser(int idUser, int[] categoriesId){
		try {
			int id = getLastIdCU();
			boolean retour = true;
			for(int i = 0; i < categoriesId.length; i++){
				retour = retour & new Update().execute("INSERT INTO categorie_utilisateur VALUES ("+(id+1+i)+", '"+categoriesId[i]+"', "+idUser+")").get();
			}
			return retour; 
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Poster msg.
	 *
	 * @param pseudo, un pseudo
	 * @param dateNaiss, Format : "1997-08-22"
	 * @param sexe, H ou F
	 * @return true, if successfully send
	 */
	public static boolean inscription(String pseudo, String dateNaiss, String sexe){
		try {
			int id = getLastIdU();
			return (new Update().execute("INSERT INTO utilisateur VALUES ("+(id+1)+", '"+pseudo+"', '"+dateNaiss+"', '"+sexe+"')").get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return false;
	}
	/**
	 *
	 * Enregistre n nouvelles catégories pour l'utilisateur.
	 *
	 * @param idUser the id user
	 * @param idLieu the id ville
	 * @return true, if successful
	 */
	public static boolean ajoutLieuUser(int idUser, int idLieu){
		try {
			int id = getLastIdVU();
			return new Update().execute("INSERT INTO lieu_utilisateur VALUES ("+(id+1)+", "+idLieu+", "+idUser+")").get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return false;
	}
	/**
	 *
	 * Enregistre n nouvelles catégories pour l'utilisateur.
	 * (
	 * @param idUser the id user
	 * @return true, if successful
	 */
	public static ArrayList<String[]> getLieuUser(int idUser){
		try {
			return new Select().execute("SELECT DISTINCT idLieu FROM lieu_utilisateur "
					+ "WHERE id="+idUser).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return null;
	}


	public static boolean ModifNaiss(int idUser, String dateNaiss){
		try {
			return new Update().execute("UPDATE utilisateur SET dateNaiss = '"+ dateNaiss +"' "
					+ "WHERE id="+idUser).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean ModifPseudo(int idUser, String pseudo){
		try {
			return new Update().execute("UPDATE utilisateur SET pseudo = '"+ pseudo +"' "
					+ "WHERE id="+idUser).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean ModifSexe(int idUser, String sexe){
		try {
			return new Update().execute("UPDATE utilisateur SET sexe = '"+ sexe +"' "
					+ "WHERE id="+idUser).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean ModifLieu(int idUser, int idLieu){
		try {
			return new Update().execute("UPDATE lieu_utilisateur SET idLieu = "+ idLieu +" "
					+ "WHERE idUser="+idUser).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return false;
	}

}
