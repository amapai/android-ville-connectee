package com.example.icity.models;

/**
 *  Classe simulant le réseau dans ta BD Thomas,
 *  j'en avais juste besoin pour mes affichages !
 */
public class Network {

    private String nom;

    public Network(String name) {
        this.nom = name;
    }

    public String getNom() {
        return this.nom;
    }

    @Override
    public String toString() {
        return "Network{" +
                "nom='" + nom + '\'' +
                '}';
    }
}
