package com.example.icity.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.icity.R;
import com.example.icity.models.Post;

import java.util.ArrayList;

/**
 * Class that links Post's object with their layout in item_post.xml.
 * Diplays nicely our post in concerned activity.
 */
public class PostAdapter extends ArrayAdapter<Post> {

    public PostAdapter(Context context, ArrayList<Post> posts) {
        super(context, 0, posts);
    }

    /**
     * Build our post View.
     *
     * @param position of the post among the others
     * @param convertView use to convert our data into the new View.
     * @param parent the View in which our new View item will be stored
     * @return updated View
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Post post = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_post, parent, false);
        }
        // Lookup view for data population
        TextView tvAuthor = (TextView) convertView.findViewById(R.id.tvAuthor);
        TextView tvContent = (TextView) convertView.findViewById(R.id.tvContent);
        // Populate the data into the template view using the data object
        tvAuthor.setText(post.getAuthor());
        tvContent.setText(post.getContent());
        // Return the completed view to render on screen
        return convertView;
    }
}
