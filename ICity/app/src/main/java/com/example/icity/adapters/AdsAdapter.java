package com.example.icity.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.icity.R;
import com.example.icity.models.Ad;

import java.util.ArrayList;

/**
 * Class that links Ad's object with their layout in item_ad.xml.
 * Diplays nicely our post in the DealsActivity.
 */
public class AdsAdapter extends ArrayAdapter<Ad> {

    public AdsAdapter(Context context, ArrayList<Ad> posts){
        super(context,0,posts);
    }

    /**
     * Build our post View.
     *
     * @param position of the post among the others
     * @param convertView use to convert our data into the new View.
     * @param parent the View in which our new View item will be stored
     * @return updated View
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        // Get the data item for this position
        Ad ad=getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if(convertView==null){
        convertView= LayoutInflater.from(getContext()).inflate(R.layout.item_ad,parent,false);
        }
        // Lookup view for data population
        TextView tvLabel=(TextView)convertView.findViewById(R.id.tvLabel);
        TextView tvContent=(TextView)convertView.findViewById(R.id.tvContent);
        // Populate the data into the template view using the data object
        tvLabel.setText("#"+ad.getLabel());
        tvContent.setText(ad.getContent());
        // Return the completed view to render on screen
        return convertView;
    }
}