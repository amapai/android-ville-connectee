package com.example.icity.models.API_FOR_MOBILE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import com.example.icity.models.API_TO_PHP.Select;
import com.example.icity.models.API_TO_PHP.Test;
import com.example.icity.models.API_TO_PHP.Update;

/** TODO
 * The Class ReseauSociaux.
 */
public class ReseauSociaux {
	
	public static void main(String[] args){
		
		//System.out.println(ajouterReseau(0,"test1",0,true));
		//System.out.println(ajouterReseau("Terre","Test2","mode",false));
		System.out.println(demandeAdherance(0, 1));
		System.out.println(posterMsg(1,0,"Wow en effet il fait super beau !"));
		System.out.println();
		Test.afficher(getPublicationsPost(0));
		System.out.println();
		Test.afficher(getInfosReseauxByCate(1));
		System.out.println();
		Test.afficher(getInfosReseaux());
		System.out.println();
		Test.afficher(getInfosReseau(0));
		System.out.println();
	}
	//
	public static int getLastIdRS(){
		try {
			ArrayList<String[]> ret = new Select().execute("SELECT max(id) FROM reseau_social").get();
			return ret != null?Integer.parseInt(ret.get(0)[0]):-1;
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return -1;
	}

	/** 
	 * Ajouter reseau.
	 * 
	 * @param idLieu the lieu
	 * @param nomReseau le nom reseau
	 * @param idCategorie le categorie
	 * @param estPublic si le groupe est public
	 * @return true, if successful
	 */
	public static boolean ajouterReseau(int idLieu, String nomReseau, int idCategorie, boolean estPublic){
		try {
			int id = getLastIdRS();
			int visibilite = estPublic?1:0;
			return (new Update().execute("INSERT INTO reseau_social VALUES ("+(id+1)+", "+idLieu+", '"+nomReseau+"', "+idCategorie+", "+visibilite+")").get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	/** 
	 * Ajouter reseau.
	 * 
	 * @param lieu the lieu
	 * @param nomReseau le nom reseau
	 * @param categorie le categorie
	 * @param estPublic si le groupe est public
	 * @return true, if successful
	 */
	public static boolean ajouterReseau(String lieu, String nomReseau, String categorie, boolean estPublic){
		try {
			int id = getLastIdRS();
			int idCategorie = Categorie.getIdCategorieByName(categorie);
			int idLieu = Lieu.getIdLieuByName(lieu);
			int visibilite = estPublic?1:0;
			return (new Update().execute("INSERT INTO reseau_social VALUES ("+(id+1)+", "+idLieu+", '"+nomReseau+"', "+idCategorie+", "+visibilite+")").get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	/** 
	 * Demande adherance au groupe idReseau
	 *
	 * @param idUser, id de user
	 * @param idReseau, id du reseau
	 * @return true, if successfully ask
	 */
	public static boolean demandeAdherance(int idUser, int idReseau){
		try {
			int id = getLastIdRS();
			return (new Update().execute("INSERT INTO reseau_social_user VALUES ("+(id+1)+", "+idUser+", "+idReseau+")").get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return false;
	}	
	
	/**
	 * Poster msg.
	 *
	 * @param idUser, id user
	 * @param idReseau, id reseau
	 * @param msg, le msg
	 * @return true, if successfully send
	 */
	public static boolean posterMsg(int idUser, int idReseau, String msg){
		try {
			int id = getLastIdRSP();
			return (new Update().execute("INSERT INTO reseau_social_publication VALUES ("+(id+1)+", "+idReseau+", "+idUser+", '"+msg+"')").get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return false;
	}

	private static int getLastIdRSP() {
		try {
			ArrayList<String[]> ret = new Select().execute("SELECT max(id) FROM reseau_social_publication").get();
			return ret != null?Integer.parseInt(ret.get(0)[0]):-1;
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return -1;
	}


	/**
	 * Gets infos of a reseau
	 *
	 * @return the infos reseau
	 */
	public static ArrayList<String[]> getInfosReseau(int idReseau){
		try {
			return new Select().execute("select * from reseau_social WHERE id="+idReseau).get();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Gets infos of all reseaux
	 *
	 * @return the infos
	 */
	public static ArrayList<String[]> getInfosReseaux(){
		try {
			return new Select().execute("select * from reseau_social").get();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/** TODO
	 * Gets infos of 
	 *
	 * @param idCategorie the id categorie
	 * @return the infos
	 */
	public static ArrayList<String[]> getInfosReseauxByCate(int idCategorie){
		try {
			new Select().execute("select * from reseau_social JOIN categorie ON categorie.id = reseau_social.idCategorie WHERE idCategorie="
							+ idCategorie).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Get publication
	 *
	 * @param idRS the id of social networks
	 * @return the infos
	 */
	public static ArrayList<String[]> getPublicationsPost(int idRS){
		try {
			return new Select().execute("select * from reseau_social_publication WHERE idRS="+idRS).get();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * id(RS), idLieu, nom, idCategorie, estPublique(1 ou 0), id(RSUser), idUser, idReseau
	 */
    public static ArrayList<String[]> getReseauUser(int idUser){
        try {
            return new Select().execute("select * from reseau_social JOIN reseau_social_user "
                    + " ON reseau_social.id = reseau_social_user.idReseau "
                    + " WHERE reseau_social_user.idUser=" + idUser).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * id(RS), idLieu, nom, idCategorie, estPublique(1 ou 0), id(RSUser), idUser, idReseau
     *
     * @param idCategorie the id categorie
     * @return the infos
     */
    public static ArrayList<String[]> getInfosReseauxByCateAndUser(int idCategorie, int idUser){
        try {
            new Select().execute("select * from reseau_social JOIN reseau_social_user "
                    + " ON reseau_social.id = reseau_social_user.idReseau"
                    + " WHERE reseau_social_user.idUser=" + idUser + " AND reseau_social.idCategorie="+idCategorie).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

	public static int getIDReseauxByNom(String nom){
		try {
			ArrayList<String[]> ret = new Select().execute("select id FROM reseau_social WHERE nom='"+nom+"'").get();
			return ret!=null&&!ret.isEmpty()?Integer.parseInt(ret.get(0)[0]):-1;
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return -1;
	}
}
