package com.example.icity.models.API_FOR_MOBILE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import com.example.icity.models.API_TO_PHP.Select;
import com.example.icity.models.API_TO_PHP.Test;
import com.example.icity.models.API_TO_PHP.Update;

public class Publicite {

	public static void main(String[] arg){
		System.out.println(addPub(2, "Quand vous naviguez ne manquez pas de RAM"));
		Test.afficher(getAllPubs());
		System.out.println();

		supprPub(getLastIdPub());
		Test.afficher(getPubsByCategorie(2));
	}//
	public static int getLastIdPub(){
		try {
			ArrayList<String[]> ret = new Select().execute("SELECT max(id) FROM pub").get();
			return ret != null?Integer.parseInt(ret.get(0)[0]):-1;
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return -1;
	}
	/**
	 * Add one ad
	 *
	 * @param idCategorie l'id du lieu
	 * @param contenu son nom
	 * @param idCategorie l'id de la categorie
	 */
	public static boolean addPub(int idCategorie, String contenu){
		try {
			int id = getLastIdPub();
			return (new Update().execute("INSERT INTO pub VALUES ("+(id+1)+", "+idCategorie+", '"+contenu+"')").get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return false;
	}

	
	/**
	 * suppr one ad
	 *
	 * @param id l'id du lieu
	 */
	public static boolean supprPub(int id){
		try {
			return (new Update().execute("DELETE FROM pub WHERE id="+id).get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Gets the all pubs.
	 *
	 * @return all pubs
	 * (id, idCategorie, Contenu)
	 */
	public static ArrayList<String[]> getAllPubs(){
		try {
			return new Select().execute("select * from pub").get();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/** 
	 * Gets pubs by categorie.
	 *
	 * @param idCategorie idCategorie
	 * @return pubs by categorie
	 */
	public static ArrayList<String[]> getPubsByCategorie(int idCategorie){
		try {
			return new Select().execute("select * from pub WHERE idCategorie ="+idCategorie).get();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}
}
