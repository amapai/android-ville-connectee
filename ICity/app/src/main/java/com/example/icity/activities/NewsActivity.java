package com.example.icity.activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.icity.R;
import com.example.icity.fragments.NewPostFragment;
import com.example.icity.models.API_FOR_MOBILE.Utilisateur;
import com.example.icity.utils.UtilsActivity;

import java.util.ArrayList;

/**
 * Class for the news activity. Use a fragment adapter :
 * a main page for all posts from any user's interest
 * a page by interest
 * TODO ajouter météo, trafic...
 */
public class NewsActivity extends UtilsActivity {

    // Variables for fragment
    private NewsActivity.MyPagerAdapter mAdapter;
    private ViewPager mPager;
    private LinearLayout ll;
    private int cpt;
    private Button btn;

    // Variable for lisview
    ArrayList<String[]> categories = new ArrayList<>();
    private int NUM_ITEMS = 0;

    /**
     * Called at the creation of the activity. Allow the navigation drawer to be displayed and
     * page adapter.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        makeToolbar();
        makeNavMenu(NewsActivity.this, savedInstanceState); // for navigation drawer

        if(Utilisateur.getCategorieUser(userId) != null&&userId!=0) {
            categories = Utilisateur.getCategorieUser(userId);
            NUM_ITEMS = categories.size();
            makePageAdapter(); // for the page adapter
        } else {
            TextView tv = new TextView(this);
            tv.setText("Ajoutez des centres d'intérêts !");
            tv.setTextSize(18);
            ll = findViewById(R.id.news_btn);
            ll.addView(tv);
        }
    }


    // *********************************************************************************************
    // ******************************* PAGE ADAPTER ************************************************
    // *********************************************************************************************

    /**
     *  Create the pager.
     *  Get the libelle and create the buttons associated.
     */
    public void makePageAdapter(){
        mAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mPager = findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);

        // cree un bouton pour y acceder avec le correct label
        // ajoute le onclick listener qui va bien
        // Watch for button clicks.
        ll = findViewById(R.id.news_btn);
        cpt = 0;
        for (String[] interest : categories) {
            btn = new Button(this);
            btn.setText("#" + interest[0]);
            final int finalCpt = cpt;
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPager.setCurrentItem(finalCpt);
                }
            });
            cpt++;
            ll.addView(btn);
        }
    }

    /**
     * Class of adapter for the activity.
     */
    private class MyPagerAdapter extends FragmentStatePagerAdapter {

        MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        /**
         * Call the list fragment @NewsPostFragment in fragments folder.
         * @param position indice of the wanted fragment to display
         * @return corresponding fragment
         */
        @Override
        public Fragment getItem(int position) {
            return NewPostFragment.newInstance(position, categories.get(position)[0]);
        }
    }
}
