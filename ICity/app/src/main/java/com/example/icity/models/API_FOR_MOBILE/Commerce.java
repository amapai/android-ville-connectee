package com.example.icity.models.API_FOR_MOBILE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import com.example.icity.models.API_TO_PHP.Select;
import com.example.icity.models.API_TO_PHP.Test;

public class Commerce {
	public static void main(String[] arg){
		String [] tmp = getInfoCommerce(0);
		for(int i = 0; i < tmp.length;i++){
			System.out.print(tmp[i]+" ");
		}
		System.out.println();
		
		Test.afficher(getInfoAllCommerce());
		System.out.println();
		
		Test.afficher(getNewsCommerce(0));
		System.out.println();
		Test.afficher(getNewsCommerce(1));
		System.out.println();
		Test.afficher(getCommercesParCategorie("restauration"));
		System.out.println();
		Test.afficher(getCommercesParIdCategorie(4));
		System.out.println();
		Test.afficher(getAllNewsCommerce());
	}
	
	/**
	 * 
	 * Renvoi les informations d'un commerce.
	 *
	 * @param idCommerce l'id du commerce
	 * @return ses infos
	 */
	public static String[] getInfoCommerce(int idCommerce){
		try {
			return new Select().execute("SELECT * FROM commerce WHERE id="+idCommerce).get().get(0);
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 
	 * Renvoi les info de tout les commerces.
	 *
	 * @return the info all commerce
	 */
	public static ArrayList<String[]> getInfoAllCommerce(){
		try {
			return new Select().execute("SELECT * FROM commerce").get();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * TODO
	 * Renvoi les news de tout les commerces.
	 * 
	 * @return les new du commerce
	 */
	public static ArrayList<String[]> getAllNewsCommerce(){
		return new Select().doInBackground("SELECT * FROM commerce "
							+"JOIN commerce_news ON commerce.id = commerce_news.idCommerce");
	}

	/**
	 * TODO
	 * Renvoi les news de tout les commerces.
	 * 
	 * @param idCommerce l'id du commerce
	 * @return les new du commerce
	 */
	public static ArrayList<String[]> getNewsCommerce(int idCommerce){
		try {
			return new Select().execute("SELECT * FROM commerce "
								+"JOIN commerce_news ON commerce.id = commerce_news.idCommerce").get();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * TODO
	 *  Renvoi les info des commerces par idCategorie.
	 *
	 * @param idCategorie la categorie
	 * @return les info des commerces correspondants 
	 */
	public static ArrayList<String[]> getCommercesParCategorie(int idCategorie){
		try {
			return new Select().execute("SELECT * FROM commerce "
								+"JOIN commerce_news ON commerce.id = commerce_news.idCommerce "
								+"WHERE commerce.idCategorie ="+idCategorie).get();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * TODO
	 * Renvoi les commerces par categorie.
	 *
	 * @param categorie la categorie
	 * @return les info des commerces correspondants 
	 */
	public static ArrayList<String[]> getCommercesParCategorie(String categorie){
		try {
			return new Select().execute("SELECT * FROM commerce "
								+"JOIN categorie ON commerce.idCategorie = categorie.id "
								+"WHERE categorie.libele='"+categorie+"'").get();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * TODO
	 * Renvoi les commerces par categorie.
	 *
	 * //@param categorie la categorie
	 * @return les info des commerces correspondants 
	 */
	public static ArrayList<String[]> getCommercesParIdCategorie(int idCategorie){
		try {
			return new Select().execute("SELECT * FROM commerce WHERE idCategorie="+idCategorie).get();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * TODO
	 * Renvoi les commerces par categorie.
	 *
	 * //@param categorie la categorie
	 * @return les info des commerces correspondants
	 */
	public static ArrayList<String[]> getCommercesParIdLieu(int idLieu){
		try {
			return new Select().execute("SELECT * FROM commerce WHERE idLieu="+idLieu).get();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * TODO
	 * Renvoi les commerces les plus proches
	 * 
	 * @param lon la longitude
	 * @param lat la latitue
	 * @return
	 */
	public ArrayList<String[]> rechercheProxy(double lon, double lat){
		return null;
	}
}
