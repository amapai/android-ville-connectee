package com.example.icity.activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.icity.R;
import com.example.icity.fragments.NetworkPostFragment;
import com.example.icity.models.API_FOR_MOBILE.ReseauSociaux;
import com.example.icity.utils.UtilsActivity;

import java.util.ArrayList;

/**
 * Class for network's posts activity. The user can see a page by network.
 */
public class NetworksNewsActivity extends UtilsActivity {

    // Variables for fragment
    private ArrayList<String[]> networks = new ArrayList<>();
    private int NUM_ITEMS = 0;
    private NetworksNewsActivity.MyPagerAdapter mAdapter;
    private ViewPager mPager;
    private LinearLayout ll;
    private int cpt;
    private Button btn;
    private EditText groupET;
    private EditText contentET;
    private Button post;

    /**
     * Called at the creation of the activity. Allow the navigation drawer to be displayed and
     * page adapter.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_news);

        makeToolbar();
        makeNavMenu(NetworksNewsActivity.this, savedInstanceState); // for navigation drawer
        if(ReseauSociaux.getReseauUser(userId) != null&&userId!=0) {
            networks = ReseauSociaux.getReseauUser(userId);
            NUM_ITEMS = networks.size();
            makePageAdapter();

        } else {
            TextView tv = new TextView(this);
            tv.setText("Inscrivez vous à réseau social !");
            tv.setTextSize(18);
            ll = findViewById(R.id.news_btn);
            ll.addView(tv);
        }
        makePostAvailable();
    }


    // *********************************************************************************************
    // ******************************* PAGE ADAPTER ************************************************
    // *********************************************************************************************

    /**
     * Create the pager.
     * Get networks' names and create the buttons associated.
     * TODO get networks name
     */
    public void makePageAdapter(){
        mAdapter = new NetworksNewsActivity.MyPagerAdapter(getSupportFragmentManager());
        mPager = (ViewPager)findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);

        ll = (LinearLayout) findViewById(R.id.news_btn);
        cpt = 0;
        for (String[] network : networks) {
            btn = new Button(this);
            btn.setText("#" + network[2]);

            final int finalCpt = cpt;
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPager.setCurrentItem(finalCpt);
                }
            });
            cpt++;
            ll.addView(btn);
        }
    }

    /**
     * Class of adapter for the activity.
     */
    private class MyPagerAdapter extends FragmentStatePagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {
            return NetworkPostFragment.newInstance(position, networks.get(position)[2]);
        }
    }

    public void makePostAvailable(){
        groupET = findViewById(R.id.my_group_et);
        contentET = findViewById(R.id.my_post_et);
        post = findViewById(R.id.btn_post);
        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String group = groupET.getText().toString();
                String content = contentET.getText().toString();

                int id = -1;
                if(networks!=null) {
                    for(String[] network : networks) {
                        if(network[2].equals(group)) {
                            id = ReseauSociaux.getIDReseauxByNom(group);
                            ReseauSociaux.posterMsg(userId,id,content);
                        }
                    }
                    if(id==-1){
                        Toast.makeText(NetworksNewsActivity.this, "Vous n'avez pas accès à ce réseau.",Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(NetworksNewsActivity.this, "Vous n'appartenez à aucun réseau",Toast.LENGTH_LONG).show();
                }

            }
        });

    }
}
