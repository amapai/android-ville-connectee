package com.example.icity.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.icity.R;
import com.example.icity.activities.DealsActivity;
import com.example.icity.activities.InterestPreferencesActivity;
import com.example.icity.activities.NetworkSearchActivity;
import com.example.icity.activities.NetworksNewsActivity;
import com.example.icity.activities.NewsActivity;
import com.example.icity.activities.ProfileActivity;
import com.example.icity.activities.ShopNewsActivity;
import com.example.icity.activities.ShopSearchActivity;
import com.example.icity.models.API_FOR_MOBILE.Utilisateur;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class UtilsActivity extends AppCompatActivity {

    // Navigation drawer
    DrawerLayout drawerLayout;
    Intent menuIntent;

    // Toolbar
    Toolbar toolbar;
    ActionBar actionbar;

    public int userId;
    public String userName;

    public Bundle mySavedInstanceState = new Bundle();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mySavedInstanceState = getIntent().getExtras();
        if(mySavedInstanceState==null){
            mySavedInstanceState = new Bundle();
        }
        String userName = mySavedInstanceState.getString("USER_NAME");

        if(userName == null){
            // user is not connected, check logs in file to get name
            String savedData = readSavedData();
            if(!savedData.equals("")){
                String datas[] = savedData.split("-");
                for (String d : datas) {
                    if(d.contains("USER_NAME")){
                        String name[] = d.split("=");
                        userName= name[1];
                    }
                }
                if (userName != null){
                    mySavedInstanceState.putString("USER_NAME", userName);
                }
            }
            else{
                // first time user uses app, let's call him Indefini
                userName = "Anonyme";
                mySavedInstanceState.putString("USER_NAME", userName);
                writeData("USER_NAME = "+userName);
                Utilisateur.inscription(userName, "2000-01-01", "H");
            }
        }
        userId = Utilisateur.connexion(userName);
        mySavedInstanceState.putInt("USER_ID", userId);
    }

    /**
     * Create the toolbar
     */
    public void makeToolbar(){
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
    }

    /**
     * Allow the navigation menu on the left to slide.
     */
    public void makeNavMenu(final Activity currentActivity, final Bundle bundle){
        drawerLayout = findViewById(R.id.drawer_layout);

        final NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        drawerLayout.closeDrawers();

                        switch (menuItem.getItemId()) {
                            case R.id.nav_profile:
                                menuIntent = new Intent(currentActivity, ProfileActivity.class);
                                menuIntent.putExtras(mySavedInstanceState);
                                startActivity(menuIntent, mySavedInstanceState);
                                return true;
                            case R.id.nav_interest:
                                menuIntent = new Intent(currentActivity, InterestPreferencesActivity.class);
                                menuIntent.putExtras(mySavedInstanceState);
                                startActivity(menuIntent, mySavedInstanceState);
                                return true;
                            case R.id.nav_actu:
                                menuIntent = new Intent(currentActivity, NewsActivity.class);
                                menuIntent.putExtras(mySavedInstanceState);
                                startActivity(menuIntent, mySavedInstanceState);
                                return true;
                            case R.id.nav_good_deal:
                                menuIntent = new Intent(currentActivity, DealsActivity.class);
                                menuIntent.putExtras(mySavedInstanceState);
                                startActivity(menuIntent, mySavedInstanceState);
                                return true;
                            case R.id.nav_shop_search:
                                menuIntent = new Intent(currentActivity, ShopSearchActivity.class);
                                menuIntent.putExtras(mySavedInstanceState);
                                startActivity(menuIntent, mySavedInstanceState);
                                return true;
                            case R.id.nav_shop_news:
                                menuIntent = new Intent(currentActivity, ShopNewsActivity.class);
                                menuIntent.putExtras(mySavedInstanceState);
                                startActivity(menuIntent, mySavedInstanceState);
                                return true;
                            case R.id.nav_networks:
                                menuIntent = new Intent(currentActivity, NetworksNewsActivity.class);
                                startActivity(menuIntent, mySavedInstanceState);
                                return true;
                            case R.id.nav_discover_community:
                                menuIntent = new Intent(currentActivity, NetworkSearchActivity.class);
                                menuIntent.putExtras(mySavedInstanceState);
                                startActivity(menuIntent, mySavedInstanceState);
                                return true;
                            default:
                                return false;
                        }
                    }
                });
    }

    /**
     * Open the navigation menu when clicked on button.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void writeData ( String data ) {
        try {
            FileOutputStream fOut = openFileOutput("settings.dat", MODE_WORLD_READABLE);
            OutputStreamWriter osw = new OutputStreamWriter(fOut);
            osw.write(data);
            osw.flush();
            osw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String readSavedData ( ) {
        StringBuffer datax = new StringBuffer("");
        try {
            FileInputStream fIn = openFileInput ( "settings.dat" ) ;
            InputStreamReader isr = new InputStreamReader ( fIn ) ;
            BufferedReader buffreader = new BufferedReader ( isr ) ;

            String readString = buffreader.readLine ( ) ;
            while ( readString != null ) {
                datax.append(readString);
                readString = buffreader.readLine ( ) ;
            }

            isr.close ( ) ;
        } catch ( FileNotFoundException fnfe ) {
            fnfe.printStackTrace ( ) ;
        } catch ( IOException ioe ) {
            ioe.printStackTrace ( ) ;
        }
        return datax.toString() ;
    }
}
