package com.example.icity.models.API_FOR_MOBILE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import com.example.icity.models.API_TO_PHP.Select;
import com.example.icity.models.API_TO_PHP.Update;

public class Lieu {

	public static int getIdLieuByName(String lieu) {
		try {
			ArrayList<String[]> ret = new Select().execute("select id from lieu WHERE nom='"+lieu+"'").get();
			return ret != null&&!ret.isEmpty()?Integer.parseInt(ret.get(0)[0]):-1;
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return -1;
	}

	public static String getNomLieuById(int idLieu) {
		try {

			return new Select().execute("select nom from lieu WHERE id="+idLieu+"").get().get(0)[0];
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return "No-Where";
	}


	public static ArrayList<String[]> getInfoLieuById(int idLieu) {
		try {

			return new Select().execute("select * from lieu WHERE id="+idLieu+"").get();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static int addLieu(int idLieuContenuPar, String nom) {
		try {
			int id = getLastIdLieu();
			boolean ret =(new Update().execute("INSERT INTO lieu VALUES ("+(id+1)+", "+idLieuContenuPar+", '"+nom+"')").get());
			if(ret){
				return getIdLieuByName(nom);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return -1;
	}

	public static int getLastIdLieu(){
		try {
			return Integer.parseInt(new Select().execute("SELECT max(id) FROM lieu").get().get(0)[0]);
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return -1;
	}
}
