package com.example.icity.fragments;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.icity.R;
import com.example.icity.activities.ShopNewsActivity;
import com.example.icity.adapters.PostAdapter;
import com.example.icity.models.API_FOR_MOBILE.Categorie;
import com.example.icity.models.API_FOR_MOBILE.Commerce;
import com.example.icity.models.API_FOR_MOBILE.Utilisateur;
import com.example.icity.models.Post;

import java.util.ArrayList;

/**
 * Class for the post's fragment. A fragment is a page of the activity.
 * Labelled ALL or CategorieLibelle.
 * In each page, posts from shops depending on their categories are displayed.
 */
public class ShopPostFragment extends ListFragment {
    int mNum;
    String label;
    ArrayList<Post> postList = new ArrayList<>();

    /**
     * Create a new instance of CountingFragment, providing "num"
     * as an argument.
     */
    public static ShopPostFragment newInstance(int num, String label) {
        ShopPostFragment f = new ShopPostFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("num", num);
        args.putString("label", label);
        f.setArguments(args);

        return f;
    }

    /**
     * When creating, retrieve this instance's number from its arguments.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNum = getArguments() != null ? getArguments().getInt("num") : 1;
        label = getArguments() != null ? getArguments().getString("label") : "ALL";
    }

    /**
     * The Fragment's UI is just a simple text view showing its
     * instance number.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragmentpager_news, container, false);
        View tv = v.findViewById(R.id.text);
        ((TextView) tv).setText("#"+label);
        return v;
    }

    /**
     * Populates our fragment UI with Posts.
     *
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getPosts();

        super.onActivityCreated(savedInstanceState);
        setListAdapter(new PostAdapter(getActivity(), postList));
    }

    /**
     * TODO définir ce qui se passe quand on clique
     *
     * @param l
     * @param v
     * @param position
     * @param id
     */
    public void onListItemClick(ListView l, View v, int position, long id) {
        Log.i("FragmentList", "Item clicked: " + id);
    }

    /**
     * Simulate the gathering of posts
     */
    private void getPosts(){
        // get categoryID
        int categoryId = Categorie.getIdCategorieByName(label);
        ArrayList<String[]> posts = new ArrayList<>();

        // get posts by shops
        posts.addAll(getShopPosts(categoryId));

        for (String[] p:posts) {
            Post postListItem = new Post(Utilisateur.getInfoUser(Integer.parseInt(p[0]))[1], p[2]);
            postList.add(postListItem);
        }
    }

    /**
     * Look for the posts published by shops related to a category
     * @param categoryId : id of the willing category
     * @return an array of posts in a weird format
     */
    private ArrayList<String[]> getShopPosts(int categoryId) {
        ArrayList<String[]> posts = new ArrayList<>();
        ArrayList<String[]> shops = Commerce.getCommercesParIdCategorie(categoryId);
        if(shops != null){
            for (String[] shop : shops) {
                Toast.makeText(getContext(), shop[0],Toast.LENGTH_LONG).show();

                posts.addAll(Commerce.getNewsCommerce(Integer.parseInt(shop[0])));
            }
        }
        return posts;
    }
}

