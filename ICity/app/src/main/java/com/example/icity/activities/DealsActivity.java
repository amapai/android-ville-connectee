package com.example.icity.activities;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.example.icity.R;
import com.example.icity.adapters.AdsAdapter;
import com.example.icity.models.API_FOR_MOBILE.Categorie;
import com.example.icity.models.API_FOR_MOBILE.Publicite;
import com.example.icity.models.API_FOR_MOBILE.Utilisateur;
import com.example.icity.models.Ad;
import com.example.icity.utils.UtilsActivity;

import java.util.ArrayList;

/**
 * Class for the "deals" and advertisment activity.
 * Use a fragment pager : a page by shop followed (or all the ads in a single
 * page ?).
 */
public class DealsActivity extends UtilsActivity {

    // Variables for ListAdapter
    private ArrayList<Ad> adList = new ArrayList<>();
    TableLayout ll;


    /**
     * Called at the creation of the activity. Allow the navigation drawer to be displayed and
     * page adapter.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deals);

        makeToolbar();
        makeNavMenu(DealsActivity.this, savedInstanceState); // for navigation drawer
        makeListAdaptater(); // for the listAdapter
    }


    // *********************************************************************************************
    // ******************************* LIST ADAPTER ************************************************
    // *********************************************************************************************

    /**
     * Create the ListAdapter.
     * Get ads and adapt them in the view (with an AdsAdapter).
     */
    public void makeListAdaptater(){
        getAds();
        ListView listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(new AdsAdapter(this, adList));
    }

    private void getAds(){
        ArrayList<String[]> categories = Utilisateur.getCategorieUser(userId);
        ArrayList<String[]> ads = new ArrayList<>();
        if(categories!= null) {
            int idCat = 0;
            for (String[] cat : categories){
                idCat = Categorie.getIdCategorieByName(cat[0]);
                if (Publicite.getPubsByCategorie(idCat) != null) {
                    ads.addAll(Publicite.getPubsByCategorie(idCat));
                }
            }

            String adCategory = "";
            for (String[] ad : ads) {
                adCategory = Categorie.getNameCategorieById(ad[1]);
                adList.add(new Ad(adCategory, ad[2]));
            }
        } else {
            TextView tv = new TextView(this);
            tv.setText("Ajoutez des centres d'intérêts !");
            tv.setTextSize(18);
            ll = findViewById(R.id.content_frame);
            ll.addView(tv);

        }
    }
}