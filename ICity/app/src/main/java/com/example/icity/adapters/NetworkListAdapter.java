package com.example.icity.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.icity.R;
import com.example.icity.models.API_FOR_MOBILE.ReseauSociaux;
import com.example.icity.models.API_FOR_MOBILE.Utilisateur;
import com.example.icity.models.Network;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class NetworkListAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater inflater;
    private int userId;
    private List<Network> networkNamesList;
    private ArrayList<Network> arraylist;

    public NetworkListAdapter(Context context, List<Network> networkNamesList, int userId) {
        mContext = context;
        this.userId = userId;
        this.networkNamesList = networkNamesList;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(networkNamesList);
    }

    public class ViewHolder {
        TextView name;
        Button btnJoin;
    }

    @Override
    public int getCount() {
        return networkNamesList.size();
    }

    @Override
    public Network getItem(int position) {
        return networkNamesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
    final NetworkListAdapter.ViewHolder holder;
        if (view == null) {
            holder = new NetworkListAdapter.ViewHolder();
            view = inflater.inflate(R.layout.item_network, null);
            // Locate the TextViews in listview_search_shopsview_search_shops.xml
            holder.name = (TextView) view.findViewById(R.id.networkName);
            holder.btnJoin = view.findViewById(R.id.networkBtn);
            holder.btnJoin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int networkId = ReseauSociaux.getIDReseauxByNom(holder.name.getText().toString());
                    ReseauSociaux.demandeAdherance(userId, networkId);
                }
            });
            view.setTag(holder);
        } else {
            holder = (NetworkListAdapter.ViewHolder) view.getTag();
        }
        // Set the results into TextViews
        holder.name.setText(networkNamesList.get(position).getNom());
        return view;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        networkNamesList.clear();
        if (charText.length() == 0) {
            networkNamesList.addAll(arraylist);
        } else {
            for (Network wp : arraylist) {
                if (wp.getNom().toLowerCase(Locale.getDefault()).contains(charText)) {
                    networkNamesList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
