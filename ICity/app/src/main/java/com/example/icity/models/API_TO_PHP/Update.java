package com.example.icity.models.API_TO_PHP;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * @author thoma
 *
 */
public class Update extends AsyncTask<String,String, Boolean> {
	/**
	 * Appel à update.php
	 * UPDATE table SET attribu=valeur WHERE 1
	 * @param cmd la rqt SQL à executer
	 * @return True si ok, false sinon
	 * @throws IOException si pb avec rqt
	 */
	public static boolean update(String cmd) throws IOException {
		String stringUrl = "http://projet-mobile-ter.alwaysdata.net/phpServ/update.php";
		if(!cmd.isEmpty()){
			stringUrl+="?cmd="+cmd.replace("[,:]", " ").replace(" ", "%20");
		} 
		System.out.println(cmd);
		
		URL url = new URL(stringUrl);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setDoOutput(true);
		con.setRequestProperty("Content-Type", "application/json");

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		boolean retour = false;
		while ((inputLine = in.readLine()) != null) {
			retour = Boolean.parseBoolean(inputLine);
		}
		in.close();
		return retour;
	}

	@Override
	protected Boolean doInBackground(String... url) {
		try {
			return update(url[0]);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
