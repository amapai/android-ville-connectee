package com.example.icity.models.API_TO_PHP;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author thoma
 *
 */
public class Select extends AsyncTask<String,String,ArrayList<String[]>>{

	/**
	 * Appel à Select.php
	 * Exemple 
	 * Select * FROM utilisateur
	 * @param cmd la rqt SQL à executer
	 * @return un tableau avec la réponse à la rqt
	 * @throws IOException si pb avec rqt
	 */
	public static ArrayList<String[]> select(String cmd) throws IOException {
		String stringUrl = "http://projet-mobile-" +
				"ter.alwaysdata.net/phpServ/select.php";
		if(!cmd.isEmpty()){
			stringUrl+="?cmd="+cmd.replace(" ", "%20");
		} 
		URL url = new URL(stringUrl);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setDoOutput(true);
		con.setRequestProperty("Content-Type", "application/json");

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		ArrayList<String> content = new ArrayList<String>();
		while ((inputLine = in.readLine()) != null) {
			if(inputLine.contains("<br />")){
				throw new IOException("Rqt SQL mauvaise : "+cmd);
			}
			content.addAll(Arrays.asList(inputLine.split("<br/>")));
		}
		in.close();
		return JsonToString(content);
	}

	public static ArrayList<String[]> JsonToString(ArrayList<String> txtJson){
		ArrayList<String[]> aReturn = new ArrayList<String[]>();
		for(int i = 0 ; i < txtJson.size(); i++){
			String[] enJson = txtJson.get(i).replaceAll("[}{\"]", "").split("[,:]");
			
			String[] content = new String[(int)enJson.length/2];
			for(int j = 0; j < enJson.length; j+=2){
				String tmp = enJson[j+1];
				/*
				try {
					byte ptext[] = tmp.getBytes("UTF8");
					content[j/2] = new String(ptext, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					content[j/2] = tmp;
				}*/
				content[j/2] = replaceUTF(tmp);
			}
			aReturn.add(content);
		}
		
		return aReturn;
		
	}

	
	public static String replaceUTF(String chaine) {
		chaine = chaine.replace("\\u00e9", "é");
		chaine = chaine.replace("\\u00e0", "à");
		chaine = chaine.replace("\\u00e8", "è");
		chaine = chaine.replace("\\u00ea", "ê");
		return chaine;
	}

	@Override
	public ArrayList<String[]> doInBackground(String... url) {
		try {
			return select(url[0]);
		} catch (IOException e) {
			e.printStackTrace();
			return null;

		}
	}
}
