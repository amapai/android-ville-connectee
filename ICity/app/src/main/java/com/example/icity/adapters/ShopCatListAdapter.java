package com.example.icity.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.icity.R;
import com.example.icity.models.Shop;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ShopCatListAdapter extends BaseAdapter {
    Context mContext;
    LayoutInflater inflater;
    private List<Shop> shopNamesList = null;
    private ArrayList<Shop> arraylist;

    public ShopCatListAdapter(Context context, List<Shop> shopNamesList) {
        mContext = context;
        this.shopNamesList = shopNamesList;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<Shop>();
        this.arraylist.addAll(shopNamesList);
    }

    public class ViewHolder {
        TextView category;
        TextView name;
        TextView city;
    }

    @Override
    public int getCount() {
        return shopNamesList.size();
    }

    @Override
    public Shop getItem(int position) {
        return shopNamesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ShopCatListAdapter.ViewHolder();
            view = inflater.inflate(R.layout.item_shop, null);
            // Locate the TextViews in item_shop.xml
            holder.category = (TextView) view.findViewById(R.id.shop_category);
            holder.name = (TextView) view.findViewById(R.id.shop_name);
            holder.city = (TextView) view.findViewById(R.id.shop_city);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        // Set the results into TextViews
        holder.category.setText("#"+shopNamesList.get(position).getCategory());
        holder.name.setText(shopNamesList.get(position).getNom());
        holder.city.setText(shopNamesList.get(position).getCity());
        return view;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        shopNamesList.clear();
        if (charText.length() == 0) {
            shopNamesList.addAll(arraylist);
        } else {
            for (Shop s : arraylist) {
                if (s.getCategory().toLowerCase(Locale.getDefault()).contains(charText)) {
                    shopNamesList.add(s);
                }
            }
        }
        notifyDataSetChanged();
    }

    /**
     * Display shops related post below the search shop place.
     */
    public void seePosts(String shopName){
        Toast.makeText(mContext.getApplicationContext(), "Faire afficher les posts liés à "+shopName, Toast.LENGTH_LONG).show();
    }
}
