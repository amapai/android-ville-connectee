package com.example.icity.activities;

import android.os.Bundle;
import android.widget.SearchView;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.example.icity.R;
import com.example.icity.adapters.ShopCatListAdapter;
import com.example.icity.adapters.ShopNearListAdapter;
import com.example.icity.models.API_FOR_MOBILE.Categorie;
import com.example.icity.models.API_FOR_MOBILE.Commerce;
import com.example.icity.models.API_FOR_MOBILE.Lieu;
import com.example.icity.models.Shop;
import com.example.icity.utils.UtilsActivity;

import java.util.ArrayList;

/**
 * Class for the shop's research activity.
 * User can look a shop in two ways :
 *  - by categories
 *  - as previous but display the nearest first
 */
public class ShopSearchActivity extends UtilsActivity implements SearchView.OnQueryTextListener {


    // Variables for research suggestions
    private ListView listCat;
    private ListView listNear;
    private TableLayout ll;
    private ShopNearListAdapter adapterNear;
    private ShopCatListAdapter adapterCat;
    private SearchView editSearch;
    private ArrayList<String[]> shops;
    private ArrayList<Shop> arraylist = new ArrayList<>();

    /**
     * Called at the creation of the activity. Allow the navigation drawer to be displayed and
     * the research view.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_search);

        makeToolbar();
        makeNavMenu(ShopSearchActivity.this, savedInstanceState); // for navigation drawer
        makeSearchingViewCat();
        makeSearchingViewNear();
    }







    // *********************************************************************************************
    // ********************************* SEARCHVIEW ************************************************
    // *********************************************************************************************

    /**
     * Create suggestions (adapter) and the searchview.
     * TODO utiliser la vraie fonction pour récupérer les listes
     * TODO faire deux fonctions pour les deux types de recherches
     */
    private void makeSearchingViewCat() {
        arraylist = new ArrayList<>();
        shops = Commerce.getInfoAllCommerce();
        if(shops!=null) {

            String category = "";
            int cityId = 0;
            String city = "";
            for(String[] shop : shops){
                category = Categorie.getNameCategorieById(shop[1]);
                cityId = Integer.parseInt(shop[3]);
                city = Lieu.getNomLieuById(cityId);
                Shop s = new Shop(category, shop[2], city);
                arraylist.add(s);
            }

            listCat = (ListView) findViewById(R.id.shop_search_cat_results);
            adapterCat = new ShopCatListAdapter(this, arraylist);
            listCat.setAdapter(adapterCat);

            editSearch = (SearchView) findViewById(R.id.shop_search);
            editSearch.setOnQueryTextListener(this);
        } else {
            TextView tv = new TextView(this);
            tv.setText("Aucun magasin disponible...");
            tv.setTextSize(18);
            ll = findViewById(R.id.content_frame);
            ll.addView(tv);
        }
    }
    /**
     * Create suggestions (adapter) and the searchview.
     * TODO utiliser la vraie fonction pour récupérer les listes
     * TODO faire deux fonctions pour les deux types de recherches
     */
    private void makeSearchingViewNear() {
        arraylist = new ArrayList<>();
        shops = Commerce.getInfoAllCommerce();
        if(shops!=null){
            String category = "";
            int cityId = 0;
            String city = "";
            for(String[] shop : shops){
                category = Categorie.getNameCategorieById(shop[1]);
                cityId = Integer.parseInt(shop[3]);
                city = Lieu.getNomLieuById(cityId);
                Shop s = new Shop(category, shop[2], city);
                arraylist.add(s);
            }

            listNear = (ListView) findViewById(R.id.shop_search_near_results);
            adapterNear = new ShopNearListAdapter(this, arraylist);
            listNear.setAdapter(adapterNear);

            editSearch = (SearchView) findViewById(R.id.shop_search);
            editSearch.setOnQueryTextListener(this);
        } else {
            TextView tv = new TextView(this);
            tv.setText("Aucun magasin disponible...");
            tv.setTextSize(18);
            ll = findViewById(R.id.content_frame);
            ll.addView(tv);
        }
    }

    /**
     * @param query
     * @return something
     */
    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    /**
     * Update suggestions according to what the user write.
     * @param newText
     * @return
     */
    @Override
    public boolean onQueryTextChange(String newText) {
        String text = newText;
        adapterCat.filter(text);
        adapterNear.filter(text);
        return false;
    }
}
