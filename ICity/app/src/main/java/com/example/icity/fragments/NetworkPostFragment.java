package com.example.icity.fragments;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.icity.R;
import com.example.icity.adapters.PostAdapter;
import com.example.icity.models.API_FOR_MOBILE.ReseauSociaux;
import com.example.icity.models.API_FOR_MOBILE.Utilisateur;
import com.example.icity.models.Post;

import java.util.ArrayList;

/**
 * Class for the post's fragment. A fragment is a page of the activity.
 * Labelled ALL or NetworkName.
 * In each page, posts from a specific network are displayed (except for the ALL categorie.
 */
public class NetworkPostFragment extends ListFragment {
    int mNum;
    String networkName;
    ArrayList<Post> postList = new ArrayList<>();

    /**
     * Create a new instance of CountingFragment, providing "num"
     * as an argument.
     */
    public static NetworkPostFragment newInstance(int num, String networkName) {
        NetworkPostFragment f = new NetworkPostFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("num", num);
        args.putString("networkName", networkName);
        f.setArguments(args);

        return f;
    }

    /**
     * When creating, retrieve this instance's number from its arguments.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNum = getArguments() != null ? getArguments().getInt("num") : 1;
        networkName = getArguments() != null ? getArguments().getString("networkName") : "ALL";
    }

    /**
     * The Fragment's UI is just a simple text view showing its
     * instance number.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragmentpager_news, container, false);
        View tv = v.findViewById(R.id.text);
        ((TextView) tv).setText(networkName);
        return v;
    }

    /**
     * Populates our fragment UI with Posts
     *
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getPosts();
        super.onActivityCreated(savedInstanceState);
        setListAdapter(new PostAdapter(getActivity(), postList));
    }

    /**
     * @param l
     * @param v
     * @param position
     * @param id
     */
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Log.i("FragmentList", "Item clicked: " + id);
    }

    /**
     * Simulate the gathering of posts
     */
    private void getPosts(){
        // get categoryID
        int networkId = ReseauSociaux.getIDReseauxByNom(networkName);
        ArrayList<String[]> posts = new ArrayList<>();

        // get pos s on networks
        posts.addAll(getNetworkPosts(networkId));

        for (String[] p:posts) {
            Post postListItem = new Post(Utilisateur.getInfoUser(Integer.parseInt(p[0]))[1], p[3]);
            postList.add(postListItem);
        }
    }

    /**
     * Look for the posts in the user networks related to a category
     * @param networkId : id of the willing category
     * @return an array of posts in a weird format
     */
    private ArrayList<String[]> getNetworkPosts(int networkId){
        ArrayList<String[]> posts = new ArrayList<>();
        if(ReseauSociaux.getPublicationsPost(networkId)!=null){
            posts.addAll(ReseauSociaux.getPublicationsPost(networkId));
        }
        return posts;
    }
}
