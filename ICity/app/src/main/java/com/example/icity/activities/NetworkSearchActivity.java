package com.example.icity.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Switch;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.icity.R;
import com.example.icity.adapters.NetworkListAdapter;
import com.example.icity.models.API_FOR_MOBILE.Categorie;
import com.example.icity.models.API_FOR_MOBILE.Lieu;
import com.example.icity.models.API_FOR_MOBILE.ReseauSociaux;
import com.example.icity.models.Network;
import com.example.icity.utils.UtilsActivity;

import java.util.ArrayList;

/**
 * Class for the shop's research activity.
 * User can look a shop in two ways :
 *  - by categories
 *  - as previous but display the nearest first
 */
public class NetworkSearchActivity extends UtilsActivity implements SearchView.OnQueryTextListener {

    // Variables for search suggestion
    private ListView list;
    private NetworkListAdapter adapter;
    private SearchView editsearch;
    private ArrayList<String[]> networks;
    private TableLayout ll;
    private ArrayList<Network> arraylist = new ArrayList<>();
    private EditText placeET;
    private EditText nameET;
    private EditText categoryET;
    private Switch isPublicS;
    private Button btnCreate;


    /**
     * Called at the creation of the activity. Allow the navigation drawer to be displayed and
     * the research view.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_search);

        makeToolbar();
        makeNavMenu(NetworkSearchActivity.this, savedInstanceState); // for navigation drawer
        makeSearchingView(); // for the page adapter
    }

    // *********************************************************************************************
    // ********************************* SEARCHVIEW ************************************************
    // *********************************************************************************************


    /**
     * Create suggestions (adapter) and the searchview.
     */
    private void makeSearchingView() {
        /* Simule le GET ALL network */
        networks = ReseauSociaux.getInfosReseaux();

        if(networks!=null) {
            for (String[] network : networks) {
                Network n = new Network(network[2]);
                // Binds all strings into an array
                arraylist.add(n);
            }

            list = (ListView) findViewById(R.id.network_search_results);
            adapter = new NetworkListAdapter(this, arraylist, userId);
            list.setAdapter(adapter);

            editsearch = (SearchView) findViewById(R.id.network_search);
            editsearch.setOnQueryTextListener(this);
        } else {
            TextView tv = new TextView(this);
            tv.setText("Aucun réseau disponible...");
            tv.setTextSize(18);
            ll = findViewById(R.id.content_frame);
            ll.addView(tv);
        }
        allowNetworkCreation();
    }

    /**
     * @param query
     * @return something
     */
    @Override
    public boolean onQueryTextSubmit(String query) {
       return false;
    }

    /**
     * Update suggestions according to what the user write.
     * @param newText
     * @return
     */
    @Override
    public boolean onQueryTextChange(String newText) {
        String text = newText;
        adapter.filter(text);
        return false;
    }

    /**
     *
     */
    public void allowNetworkCreation(){
        final EditText placeET = findViewById(R.id.network_add_category_et);
        final EditText nameET = findViewById(R.id.network_add_network_et);
        final EditText categoryET = findViewById(R.id.network_add_category_et);
        final Switch isPublicS = findViewById(R.id.network_add_network_public_s);
        btnCreate = findViewById(R.id.btn_create);

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String place = placeET.getText().toString();
                String name = nameET.getText().toString();
                String category = categoryET.getText().toString();
                boolean isPublic = isPublicS.isChecked();

                if ( place.equals("") ) {
                    Toast.makeText(NetworkSearchActivity.this, "Entrez une valeur pour le lieu svp.",Toast.LENGTH_LONG).show();
                } else {
                    // checks if city exists, adds it if not
                    if ( Lieu.getIdLieuByName(place)==-1 ) {
                        Lieu.addLieu(0, place);
                    }

                    // keep going
                    if ( name.equals("") ) {
                        Toast.makeText(NetworkSearchActivity.this, "Entrez une valeur pour le nom svp",Toast.LENGTH_LONG).show();
                    } else {
                        if ( category.equals("") ) {
                            Toast.makeText(NetworkSearchActivity.this, "Entrez une valeur pour le nom svp",Toast.LENGTH_LONG).show();
                        } else if ( Categorie.getIdCategorieByName(category)==-1 ) { // wrong value of category
                            Toast.makeText(NetworkSearchActivity.this, "Mauvaise catégorie. Vérifiez la formulation dans les préférences svp.",Toast.LENGTH_LONG).show();
                        } else { // everything is fine
                            ReseauSociaux.ajouterReseau(place, name, category, isPublic);
                            Toast.makeText(NetworkSearchActivity.this, "Réseau créé !",Toast.LENGTH_SHORT).show();

                        } // category
                    }  // name
                } // place
             }
        }); // onclick
    } // allowNetworkCreation
}
