package com.example.icity.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.icity.R;
import com.example.icity.models.API_FOR_MOBILE.Lieu;
import com.example.icity.models.API_FOR_MOBILE.Utilisateur;
import com.example.icity.utils.UtilsActivity;


/**
 * Class for the profile preference activity.
 * TODO preferences
 */
public class ProfileActivity extends UtilsActivity {

    // Variables for profile
    private ImageView ivPic;
    private EditText tvAlias;
    private EditText tvCity;
    private EditText tvSex;
    private EditText tvAge;
    private Button btnSave;

    /**
     * Called at the creation of the activity. Allow the navigation drawer to be displayed and
     * page adapter.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        final Bundle bundle = mySavedInstanceState;

        // we'll be used for save of the settings
        /*ivPic = findViewById(R.id.profile_pic);
        ArrayList<Integer> availablePics = new ArrayList<>();
        availablePics.add(R.drawable.pp_food);
        availablePics.add(R.drawable.pp_camera);
        availablePics.add(R.drawable.pp_book);
        availablePics.add(R.drawable.pp_music);
        int max = availablePics.size();
        Random randGen = new Random();
        int i = randGen.nextInt(max);
        ivPic.setImageResource(availablePics.get(i));*/
        tvAlias = findViewById(R.id.profile_pseudo);
        tvCity = findViewById(R.id.profile_city);
        tvSex = findViewById(R.id.profile_sex);
        tvAge = findViewById(R.id.profile_age);
        btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // save data in a file
                String pref = "USER_NAME = "+ tvAlias.getText().toString()
                        + " - USER_AGE = "+ tvAge.getText().toString()
                        + " - USER_SEX = "+ tvSex.getText().toString()
                        + " - USER_CITY = "+ tvCity.getText().toString()
                        + " - USER_PREF = "+ tvCity.getText().toString();
                writeData(pref);

                // put data in bundle
                bundle.putString("USER_NAME", tvAlias.getText().toString());
                bundle.putString("USER_AGE", tvAge.getText().toString());
                bundle.putString("USER_SEX", tvSex.getText().toString());
                bundle.putString("USER_CITY", tvCity.getText().toString());

                Utilisateur.ModifPseudo(userId, tvAlias.getText().toString());
                Utilisateur.ModifNaiss(userId, tvAge.getText().toString());
                Utilisateur.ModifSexe(userId, tvSex.getText().toString());
                if(Lieu.addLieu(0, tvCity.getText().toString())== -1) {
                    Lieu.addLieu(0, tvCity.getText().toString());
                }
                int cityId = Lieu.getIdLieuByName(tvCity.getText().toString());
                Utilisateur.ModifLieu(userId,cityId);

                Toast.makeText(ProfileActivity.this, "Enregistré !",Toast.LENGTH_LONG).show();
            }
        });
        mySavedInstanceState = bundle;
        makeToolbar();
        makeNavMenu(ProfileActivity.this, savedInstanceState); // for navigation drawer

        String[] infoUser = Utilisateur.getInfoUser(userId);

        if(infoUser!=null){
            tvAlias.setText(infoUser[1]);
            tvAge.setText(infoUser[2]);
            tvSex.setText(infoUser[3]);
        }
        int cityId = Lieu.addLieu(0, tvCity.getText().toString());
        if(cityId == -1) {
            tvCity.setText("À définir");
        }
        tvCity.setText(Lieu.getNomLieuById(cityId));
    }
}
