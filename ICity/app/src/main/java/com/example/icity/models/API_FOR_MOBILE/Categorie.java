package com.example.icity.models.API_FOR_MOBILE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import com.example.icity.models.API_TO_PHP.Select;
import com.example.icity.models.API_TO_PHP.Test;

public class Categorie {

	public static void main(String[] arg){
		System.out.println("id mode = "+getIdCategorieByName("mode"));
		
		Test.afficher(getCategories());
		
	}

	public static String tempo()
	{
		return "Blbblb";
	}
	/**
	 * Gets all categories.
	 *
	 * @return categories (id, nom)
	 */
	public static ArrayList<String[]> getCategories(){
		try {
			return new Select().execute("select * from categorie").get();
		} catch (ExecutionException e) {
			e.printStackTrace();
			return null;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return null;
		}
	}



	/**
	 * Gets categories by name.
	 * @return id de la catégorie
	 */
	public static int getIdCategorieByName(String categorie){
		try {
			ArrayList<String[]> ret = new Select().execute("select id from categorie WHERE libele='"+categorie+"'").get();
			return ret != null?Integer.parseInt(ret.get(0)[0]):-1;
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return -1;
	}

	/**
	 * Gets categories by name.
	 * @return id de la catégorie
	 */
	public static String getNameCategorieById(String idCat){
		try {
			ArrayList<String[]> ret = new Select().execute("select libele from categorie WHERE id="+idCat).get();
			return ret != null?ret.get(0)[0]:"vide";
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return "vide";
	}
}
