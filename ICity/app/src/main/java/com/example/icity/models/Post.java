package com.example.icity.models;

/**
 *  Classe simulant les posts,
 *  j'en avais juste besoin pour mes affichages !
 */
public class Post {
    private String author;
    private String content;

    public Post(String author, String content) {
        this.author = author;
        this.content = content;
    }

    public String getAuthor() {
        return author;
    }
    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Post{" +
                "author='" + author + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
