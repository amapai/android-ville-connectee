package com.example.icity.models;

/**
 *  Classe simulant les commerces dans ta BD Thomas,
 *  j'en avais juste besoin pour mes affichages !
 */
public class Shop {
    private String category;
    private String name;
    private String city;

    public Shop(String category, String name, String city) {
        this.category= category ;
        this.name = name;
        this.city = city;
    }

    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }

    public String getNom() {
        return name;
    }
    public void setNom(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Shop{" +
                "category='" + category + '\'' +
                ", name='" + name + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
