package com.example.icity.activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.icity.R;
import com.example.icity.fragments.ShopPostFragment;
import com.example.icity.models.API_FOR_MOBILE.Utilisateur;
import com.example.icity.utils.UtilsActivity;

import java.util.ArrayList;

/**
 * Class for the news concerning shops activity.
 * Use a fragment pager : a page by categorie of shops.
 */
public class ShopNewsActivity extends UtilsActivity {

    // Variables for fragment
    ArrayList<String[]> categories = new ArrayList<>();
    private int NUM_ITEMS = 0;

    private MyPagerAdapter mAdapter;
    private ViewPager mPager;
    private LinearLayout ll;
    private int cpt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_news);

        makeToolbar();
        makeNavMenu(ShopNewsActivity.this, savedInstanceState); // for navigation drawer

        if(Utilisateur.getCategorieUser(userId) != null&&userId!=0) {
            categories = Utilisateur.getCategorieUser(userId);
            NUM_ITEMS = categories.size();
            makePageAdapter(); // for the page adapter
        } else {
            TextView tv = new TextView(this);
            tv.setText("Ajoutez des centres d'intérêts !");
            tv.setTextSize(18);
            ll = findViewById(R.id.news_btn);
            ll.addView(tv);
        }
    }

    // *********************************************************************************************
    // ******************************* PAGE ADAPTER ************************************************
    // *********************************************************************************************

    /**
     * Create the pager.
     * Get the libelle and create the buttons associated.
     * TODO get the "libelles" (categories)
     */
    public void makePageAdapter(){
        mAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mPager = findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);

        // Pour tous les centres d'interets de l'user
        // cree un bouton pour y acceder avec le correct label
        // ajoute le onclick listener qui va bien
        // Watch for button clicks.


        ll = (LinearLayout) findViewById(R.id.news_btn);
        cpt = 0;
        for (String[] interest : categories) {
            Button btn = new Button(this);
            btn.setText("#" + interest[0]);
            final int finalCpt = cpt;
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPager.setCurrentItem(finalCpt);
                }
            });
            cpt++;
            ll.addView(btn);
        }
    }

    /**
     * Class of adapter for the activity.
     */
    private class MyPagerAdapter extends FragmentStatePagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        /**
         * Call the list fragment @DealsListFragment in fragments folder.
         * @param position
         * @return
         */
        @Override
        public Fragment getItem(int position) {
            return ShopPostFragment.newInstance(position, categories.get(position)[0]);
        }
    }
}
