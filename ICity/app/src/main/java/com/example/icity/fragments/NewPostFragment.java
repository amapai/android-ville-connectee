package com.example.icity.fragments;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.icity.R;
import com.example.icity.adapters.PostAdapter;
import com.example.icity.models.API_FOR_MOBILE.Categorie;
import com.example.icity.models.API_FOR_MOBILE.Commerce;
import com.example.icity.models.API_FOR_MOBILE.ReseauSociaux;
import com.example.icity.models.API_FOR_MOBILE.Utilisateur;
import com.example.icity.models.Post;

import java.util.ArrayList;

/**
 * Class for the new's fragment. A fragment is a page of the activity.
 * Labelled ALL or CategorieLibelle.
 * TODO ajouter meteo et traffic
 */
public class NewPostFragment extends ListFragment {
    int mNum;
    String label;
    ArrayList<Post> postList = new ArrayList<>();

    /**
     * Create a new instance of CountingFragment, providing "num"
     * as an argument.
     */
    public static NewPostFragment newInstance(int num, String label) {
        NewPostFragment f = new NewPostFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("num", num);
        args.putString("label", label);
        f.setArguments(args);

        return f;
    }

    /**
     * When creating, retrieve this instance's number from its arguments.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNum = getArguments() != null ? getArguments().getInt("num") : 1;
        label = getArguments() != null ? "#"+getArguments().getString("label") : "ALL";
    }

    /**
     * The Fragment's UI : for now just an textView as a header. For the display of posts, see
     * onActivityCreated() function.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragmentpager_news, container, false);
        View tv = v.findViewById(R.id.text);
        ((TextView)tv).setText(label);
        return v;
    }

    /**
     * Populates our fragment UI with Posts.
     *
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getPosts();

        super.onActivityCreated(savedInstanceState);
        setListAdapter(new PostAdapter(getActivity(), postList));
    }

    /**
     * TODO définir ce qui se passe quand on clique
     * @param l
     * @param v
     * @param position
     * @param id
     */
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Log.i("FragmentList", "Item clicked: " + id);
    }

    /**
     * Gather posts associated to a category.
     * We get the ID of a categories libelle, then
     *   - gather the associated posts on networks
     *   - gather the associated posts by shops
     * TODO les afficher dans le bon ordre
     * TODO ajouter meteo et traffic
     */
    private void getPosts(){
        // get categoryID
        int categoryId = Categorie.getIdCategorieByName(label);
        ArrayList<String[]> posts = new ArrayList<>();

        // get pos s on networks
        posts.addAll(getNetworkPosts(categoryId));

        // get posts by shops
        posts.addAll(getShopPosts(categoryId));

        for (String[] p:posts) {
            Post postListItem = new Post(Utilisateur.getInfoUser(Integer.parseInt(p[0]))[1], p[2]);
            postList.add(postListItem);
        }
    }

    /**
     * Look for the posts in the user networks related to a category
     * @param categoryId : id of the willing category
     * @return an array of posts in a weird format
     */
    private ArrayList<String[]> getNetworkPosts(int categoryId){
        ArrayList<String[]> posts = new ArrayList<>();

        ArrayList<String[]> networks = ReseauSociaux.getInfosReseauxByCate(categoryId);
        if(networks!=null) {
            for (String[] network : networks) {
                posts.addAll(ReseauSociaux.getPublicationsPost(Integer.parseInt(network[0])));
            }
        }
        return posts;
    }

    /**
     * Look for the posts published by shops related to a category
     * @param categoryId : id of the willing category
     * @return an array of posts in a weird format
     */
    private ArrayList<String[]> getShopPosts(int categoryId) {
        ArrayList<String[]> posts = new ArrayList<>();

        ArrayList<String[]> shops = Commerce.getCommercesParCategorie(categoryId);
         if(shops != null){
            for (String[] shop : shops) {
                posts.addAll(Commerce.getNewsCommerce(Integer.parseInt(shop[1])));
            }
        }
        return posts;
    }
}